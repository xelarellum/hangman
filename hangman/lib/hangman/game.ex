defmodule Hangman.Game do
  defstruct(
    turns_left: 7,
    game_state: :initializing,
    letters: [],
    used: []
  )

  def new_game() do
    Dictionary.random_word() |> new_game()
  end

  def new_game(word) do
    %__MODULE__{letters: word |> String.codepoints()}
  end

  def make_move(game = %{game_state: state}, _guess) when state in [:lost, :won] do
    game
    |> return_with_tally()
  end

  def make_move(game, guess) do
    make_move(game, guess, Enum.count(String.codepoints(guess)))
  end

  defp make_move(game, guess, 1) do
    guess = String.downcase(guess)

    used = MapSet.new(game.used)

    accept_move(game, guess, MapSet.member?(used, guess))
    |> return_with_tally()
  end

  defp make_move(game, _guess, _guess_length) do
    game
    |> Map.put(:game_state, :invalid_guess)
    |> return_with_tally()
  end

  defp accept_move(game, _guess, _already_guessed = true) do
    Map.put(game, :game_state, :already_used)
  end

  defp accept_move(game, guess, _already_guessed) do
    %{game | used: [guess | game.used], turns_left: game.turns_left - 1}
    |> score_guess(Enum.member?(game.letters, guess))
  end

  defp score_guess(game, _good_guess = true) do
    used = MapSet.new(game.used)

    new_state =
      MapSet.new(game.letters)
      |> MapSet.subset?(used)
      |> maybe_won(game.turns_left)

    Map.put(game, :game_state, new_state)
  end

  defp score_guess(game = %{turns_left: 0}, _not_good_guess) do
    Map.put(game, :game_state, :lost)
  end

  defp score_guess(game, _not_good_guess) do
    Map.put(game, :game_state, :bad_guess)
  end

  defp maybe_won(true, _turns_left), do: :won
  defp maybe_won(_, _turns_left = 0), do: :lost
  defp maybe_won(_, _), do: :good_guess

  defp return_with_tally(game) do
    {game, tally(game)}
  end

  def tally(game) do
    %{
      game_state: game.game_state,
      turns_left: game.turns_left,
      letters: game.letters |> reveal_letters(game.used),
      used: game.used,
      word: reveal_word(game.letters, game.game_state)
    }
  end

  def reveal_word(letters, :lost) do
    Enum.join(letters)
  end

  def reveal_word(_, _) do
    nil
  end

  defp reveal_letters(letters, used) do
    used = MapSet.new(used)

    letters
    |> Enum.map(fn letter -> reveal_letter(letter, MapSet.member?(used, letter)) end)
  end

  defp reveal_letter(letter, _in_word = true), do: letter
  defp reveal_letter(_letter, _in_word), do: "_"
end
