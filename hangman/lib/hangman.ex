defmodule Hangman do
  def new_game() do
    {:ok, pid} = Supervisor.start_child(Hangman.Supervisor, [])
    pid
  end

  def make_move(game_pid, guess) do
    game_pid
    |> GenServer.call({:make_move, guess})
  end

  def tally(game_pid) do
    game_pid
    |> GenServer.call(:tally)
  end
end
