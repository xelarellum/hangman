defmodule Hangman.GameTest do
  use ExUnit.Case
  doctest Hangman
  alias Hangman.Game

  test "new_game returns structure" do
    game = Game.new_game()

    assert game.turns_left == 7
    assert game.game_state == :initializing
    assert length(game.letters) > 0
  end

  test "new_game letters contain only lowercase letters" do
    %Game{letters: letters} = Game.new_game()
    Enum.each(letters, &assert(&1 =~ ~r"[a-z]"))
  end

  test "state unchanged for :won or :lost game" do
    for state <- [:won, :lost] do
      game = Game.new_game() |> Map.put(:game_state, state)

      assert {^game, _} = Game.make_move(game, "X")
    end
  end

  test "first occurrence of guess is not already used" do
    {game, _} = Game.new_game() |> Game.make_move("X")

    assert game.game_state != :already_used
  end

  test "second occurrence of guess is not already used" do
    {game, _} =
      Game.new_game()
      |> Game.make_move("X")

    {game, _} =
      game
      |> Game.make_move("X")

    assert game.game_state == :already_used
  end

  test "each guess decreases the remaining turns" do
    game = Game.new_game()
    {game_after_move, _} = game |> Game.make_move("X")

    assert game_after_move.turns_left < game.turns_left
  end

  test "detect a good guess" do
    game = Map.put(Game.new_game(), :letters, ["a", "b", "c"])

    {game_after_move, _} = Game.make_move(game, "a")

    assert game_after_move.game_state == :good_guess
  end

  test "detect an invalid guess (too short)" do
    game = Game.new_game()
    {game_after_move, _} = Game.make_move(game, "")

    assert game_after_move.game_state == :invalid_guess
  end

  test "detect an invalid guess (too long)" do
    game = Game.new_game()
    {game_after_move, _} = Game.make_move(game, "XX")

    assert game_after_move.game_state == :invalid_guess
  end

  defp assert_moves_for_word(moves, word) do
    game = Game.new_game(word)

    Enum.reduce(
      moves,
      game,
      fn {guess, game_state, turns_left}, game ->
        {game, _} = Game.make_move(game, guess)

        assert game.game_state == game_state
        assert game.turns_left == turns_left
        game
      end
    )
  end

  test "detect a won game" do
    [
      {"a", :good_guess, 6},
      {"b", :good_guess, 5},
      {"c", :won, 4}
    ]
    |> assert_moves_for_word("abc")
  end

  test "create a game with a predefined word" do
    game = Game.new_game("test")

    assert %Game{letters: ["t", "e", "s", "t"]} == game
  end

  test "detect a bad move" do
    {game, _} =
      Game.new_game("test")
      |> Game.make_move("x")

    assert game.game_state == :bad_guess
    assert game.turns_left == 6
  end

  test "detect a lost game" do
    [
      {"a", :bad_guess, 6},
      {"b", :bad_guess, 5},
      {"d", :bad_guess, 4},
      {"e", :bad_guess, 3},
      {"f", :bad_guess, 2},
      {"g", :bad_guess, 1},
      {"h", :lost, 0}
    ]
    |> assert_moves_for_word("x")
  end

  test "detect a lost game after a last good guess" do
    [
      {"a", :bad_guess, 6},
      {"b", :bad_guess, 5},
      {"d", :bad_guess, 4},
      {"e", :bad_guess, 3},
      {"f", :bad_guess, 2},
      {"g", :bad_guess, 1},
      {"x", :lost, 0}
    ]
    |> assert_moves_for_word("xyz")
  end

  test "detect a won game after a last good guess" do
    [
      {"a", :good_guess, 6},
      {"b", :good_guess, 5},
      {"c", :good_guess, 4},
      {"d", :good_guess, 3},
      {"e", :good_guess, 2},
      {"f", :good_guess, 1},
      {"x", :won, 0}
    ]
    |> assert_moves_for_word("abcdefx")
  end

  test "detect a won game with a short word" do
    [
      {"a", :good_guess, 6},
      {"b", :good_guess, 5},
      {"c", :won, 4}
    ]
    |> assert_moves_for_word("abc")
  end
end
