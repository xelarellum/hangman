defmodule GallowsWeb.Views.Helpers.GameStateHelper do
  # import Phoenix.HTML, only: [raw: 1]

  @responses %{
    :won => {:info, "You won!"},
    :lost => {:danger, "You lost!"},
    :good_guess => {:info, "Good guess!"},
    :bad_guess => {:warning, "Bad guess!"},
    :already_used => {:warning, "Already used!"},
    :invalid_guess => {:warning, "Invalid guess!"}
  }
  def game_state(game_state), do: alert(@responses[game_state])

  defp alert(nil), do: ""

  defp alert({class, message}) do
    """
    <p class="alert alert-#{class}" role="alert">
      #{message}
    </p>
    """
    |> Phoenix.HTML.raw()
  end
end
