defmodule SocketGallowsWeb.Channels.HangmanChannel do
  use Phoenix.Channel
  require Logger

  def join("hangman:game", _, socket) do
    game = Hangman.new_game()
    socket = assign(socket, :game, game)
    {:ok, socket}
  end

  def handle_in("tally", _, socket) do
    game = socket.assigns.game
    tally = Hangman.tally(game)
    IO.puts(inspect(tally))
    push(socket, "tally", tally)
    {:noreply, socket}
  end

  def handle_in(request, param, socket) do
    Logger.error("Unknown request #{request} with params #{inspect(param)}")
    {:noreply, socket}
  end
end
